var routes = require('../index.js')
var dataFeed = routes.dataFeed
var analysisRunner = routes.analysisRunner
var broker = routes.broker

async function main () {
  let results = await dataFeed.fetch('aapl')
  analysisRunner.analyze(results)
  broker.printAccountData()
}

main()
