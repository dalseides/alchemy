var assert = require('assert')

describe('data feed', function () {
  describe('iex', function () {
    var dataFeed = require('../data_feed/iex')

    it('is an object', function () {
      assert(dataFeed !== null && dataFeed instanceof Object)
    })
    it('has a fetch function', function () {
      assert(dataFeed.fetch !== null && dataFeed.fetch instanceof Function)
    })
    describe('lastDay', function () {
      this.timeout(10 * 1000)
      var lastDayReturn
      var moment = require('moment')
      it('has a lastDay function', function () {
        assert(dataFeed.lastDay !== null && dataFeed.fetch instanceof Function)
      })
      it('runs without erroring', async function () {
        lastDayReturn = await dataFeed.lastDay()
      })
      it('returns a moment object', function () {
        assert(lastDayReturn instanceof moment)
      })
      it('returns a recent date within 7 days', function () {
        assert(lastDayReturn.isAfter(moment().subtract(7, 'days')))
      })
    })

    it('runs without a date')
    it('runs with a date')
  })
})
