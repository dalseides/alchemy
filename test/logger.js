const assert = require('assert')
const sinon = require('sinon')
const Moment = require('moment')
const fs = require('fs')

describe('logger', function () {
  describe('consoleLogger', function () {
    let ConsoleLogger
    let fakeConsole
    before(() => {
      ConsoleLogger = require('../logger/logOutputs/consoleLogger')
      fakeConsole = {
        log: sinon.spy()
      }
    })

    let logger
    beforeEach(function () {
      logger = new ConsoleLogger(fakeConsole)
      logger.chalk.enabled = false
    })

    afterEach(function () {
      fakeConsole.log.resetHistory()
    })

    it('exists', function () {
      assert(logger.log instanceof Function)
    })

    it('calls console.log once', function () {
      logger.log('subsystem')
      assert(fakeConsole.log.calledOnce)
    })

    it('outputs the current time in hh:mm', function () {
      logger.log('subsystem')

      // Get current time for comparison
      var now = new Moment().format('HH:mm')

      assert(fakeConsole.log.getCall(0).args[0].startsWith(now))
    })

    it('outputs the subsystem', function () {
      var subsystem = 'testing'
      logger.log(subsystem)
      assert(RegExp(subsystem).test(fakeConsole.log.getCall(0).args[0]))
    })

    it('outputs message values in `key: value` format', function () {
      logger.log('subsystem', { key: 'value' })
      assert(RegExp('key: value').test(fakeConsole.log.getCall(0).args[0]))
    })
  })

  describe('csvLogger', function () {
    let CsvLogger
    let fakefs
    let fakefsexists
    before(function () {
      CsvLogger = require('../logger/logOutputs/csvLogger')

      // We don't want to actually write to a file, that's fs's job
      fakefs = sinon.stub(fs, 'appendFileSync')

      // The file isn't being created, we don't care if it exists
      fakefsexists = sinon.stub(fs, 'existsSync').returns(false)
    })

    const filename = 'tests.csv'
    let logger
    beforeEach(function () {
      logger = new CsvLogger({
        headers: ['a', 'b'],
        filename: filename
      })
    })

    it('exists', function () {
      assert(logger.log instanceof Function)
    })

    it('writes to the right file', function () {
      assert(fakefs.getCall(0).args[0] === filename)
    })

    it('adds the right headers in csv formatting', function () {
      assert(fakefs.getCall(0).args[1] === 'timestamp,subsystem,a,b\n')
    })

    it('adds a data row in csv formatting with escapes', function () {
      let now = new Moment()
      logger.log('subsystem', {
        a: 1,
        b: '"2"'
      }, now)
      assert(fakefs.getCall(1).args[1] === now.toISOString() + ',subsystem,1,"""2"""\n')
    })

    it('normalizes the order of the message', function () {
      let now = new Moment()
      logger.log('subsystem', {
        b: 2,
        a: 1
      }, now)
      assert(fakefs.getCall(1).args[1] === now.toISOString() + ',subsystem,1,2\n')
    })

    it('ignores data it doesn\'t have a header for', function () {
      let now = new Moment()
      logger.log('subsystem', {
        a: 1,
        b: 2,
        c: 3
      }, now)
      assert(fakefs.getCall(1).args[1] === now.toISOString() + ',subsystem,1,2\n')
    })

    it('can handle undefined data', function () {
      let now = new Moment()
      logger.log('subsystem', {
        a: 1
      }, now)
      assert(fakefs.getCall(1).args[1] === now.toISOString() + ',subsystem,1,\n')
    })

    afterEach(function () {
      fakefs.resetHistory()
    })

    after(function () {
      // Remove file
      fakefs.restore()
      fakefsexists.restore()
    })
  })
})
