const assert = require('assert')

const config = require('../config/config')
const tradeking = require('../broker_communication/tradeking')

describe('broker communication', function () {
  describe('tradeking', async function () {
    it('is an object', function () {
      assert(tradeking !== null && tradeking instanceof Object)
    })

    describe('account data', function () {
      var acctData
      var acctSumm

      before(async function () {
        this.timeout(10 * 1000)
        acctData = await tradeking.getAccountData()
        if (!(acctData instanceof Object)) {
          throw new Error('failed to get account data')
        }

        acctSumm = acctData.accounts.accountsummary
        if (!(acctSumm instanceof Object)) {
          throw new Error('invalid account summary format')
        }
      })

      it('got account data', function () {
        assert(acctSumm.account === config.account_id)
      })

      it('has a non-negative balance', function () {
        let acctBalance = acctSumm.accountbalance.accountvalue
        assert(!isNaN(acctBalance))
        assert(acctBalance >= 0)
      })
    })

    describe('ask preview', function () {
      let principal, error

      before(async function () {
        this.timeout(10 * 1000)

        try {
          // We have no use for output, it just needs to trigger the error
          await tradeking.sell('GE', 1)
          assert(false)
        } catch (e) {
          [principal, error] = verifyAskResponse(e)
        }
      })

      it('ask for unowned stock returned expected error response', function () {
        gotPrincipalAndError(principal, error)
      })
    })

    describe('buy preview', function () {
      let output, bidprice, askprice

      before(async function () {
        this.timeout(10 * 1000)
        output = await tradeking.buy('GE', 1);

        [askprice, bidprice] = verifyBidResponse(output)
      })

      it('got ask and bid price', function () {
        gotAskAndBidPrices(askprice, bidprice)
      })
    })
  })
})

function verifyBidResponse (output) {
  if (!(output instanceof Object)) {
    throw new Error('failed to get buy data')
  }

  let displaydata = output.response.quotes.instrumentquote.displaydata

  let bidprice = displaydata.bidprice._text
  if (typeof bidprice !== 'string') {
    throw new Error('invalid bidprice format')
  }

  let askprice = displaydata.askprice._text
  if (typeof askprice !== 'string') {
    throw new Error('invalid askprice format')
  }

  return [askprice, bidprice]
}

function gotAskAndBidPrices (askprice, bidprice) {
  assert(!isNaN(bidprice))
  assert(bidprice >= 0)

  assert(!isNaN(askprice))
  assert(askprice >= 0)
}

function verifyAskResponse (output) {
  if (!(output instanceof Object)) {
    throw new Error('failed to get buy data')
  }

  let principal = output.response.principal._text
  if (typeof principal !== 'string') {
    throw new Error('invalid principal format')
  }

  let error = output.response.error._text
  if (typeof error !== 'string') {
    throw new Error('invalid error format')
  }

  return [principal, error]
}

function gotPrincipalAndError (principal, error) {
  assert(!isNaN(principal))
  assert(principal >= 0)

  assert(typeof error === 'string')
  assert(/You are attempting to enter an order for a security which you do not hold in your account/.test(error))
}
