module.exports =
{
  analyze: function (data) {
    var averages = []
    var movingAverageShortLength = 5
    var movingAverageLongLength = 30
    var movingAverageShort = []
    var movingAverageLong = []
    var shortIsHigh = []
    var runningProfit = 0; var lastBought = 0; var lastSold = 0

    for (let minute in data) {
      averages.push(data[minute].average)

      // Short moving average
      if (minute === 0) {
        movingAverageShort.push(data[minute].average)
        movingAverageLong.push(data[minute].average)
      } else {
        let sum = 0
        let count = 0

        for (let i = minute; (i > minute - movingAverageShortLength) && (i >= 0); i--) {
          sum += data[i].average
          count++
        }

        movingAverageShort.push(sum / count)

        sum = 0
        count = 0

        for (let i = minute; (i > minute - movingAverageLongLength) && (i >= 0); i--) {
          sum += data[i].average
          count++
        }

        movingAverageLong.push(sum / count)

        shortIsHigh.push(movingAverageShort[minute] > movingAverageLong[minute])

        if (shortIsHigh.length > 1 && shortIsHigh[minute] !== shortIsHigh[minute - 1]) {
          console.log('Average: ' + averages[minute] + '\t\tS Mv Avg: ' + movingAverageShort[minute] + '\t\tL Mv Avg: ' + movingAverageLong[minute] + '\t\tshortIsHigh: ' + shortIsHigh[minute])
          if (shortIsHigh[minute]) {
            console.log('--- BUYBUYBUY ---')
            lastBought = averages[minute]
          } else {
            console.log('--- SELLSELLSELL ---')
            if (lastBought !== 0) {
              lastSold = averages[minute]
              runningProfit += lastSold - lastBought
            }
          }
        }
      }
    }
    console.log('TOTALPROFITGUYS: ' + runningProfit)
  }
}
