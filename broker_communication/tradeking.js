const oauth = require('oauth')
const xmljs = require('xml-js')
const config = require('../config/config.js')
const orderStringPrefix =
      '<?xml version="1.0" encoding="UTF-8"?> <FIXML xmlns="http://www.fixprotocol.org/FIXML-5-0-SP2">' +
      ' <Order TmInForce="0" Typ="1" '
const orderStringMiddle1 =
      'Acct="' + config.account_id + '"> <Instrmt SecTyp="CS" Sym="'
const orderStringMiddle2 =
      '"/> <OrdQty Qty="'
const orderStringSuffix =
      '"/> </Order> </FIXML>'

module.exports =
{
  getAccountData: function () {
    return new Promise(function (resolve, reject) {
      tradekingConsumer.get(config.api_url + '/accounts.json', config.access_token, config.access_secret, function (err, data, res) {
        if (data != null) {
          let acctData = JSON.parse(data)
          resolve(acctData.response)
        } else {
          reject(err)
        }
      })
    })
  },

  buy: async function (symbol, quantity, preview = true) {
    let postBody = orderStringPrefix + 'Side="1" ' + orderStringMiddle1 + symbol + orderStringMiddle2 + quantity + orderStringSuffix
    let urlSuffix = '/accounts/' + config.account_id

    urlSuffix += preview ? '/orders/preview.xml' : '/orders.xml'

    return postToTradeking(postBody, urlSuffix)
  },

  sell: async function (symbol, quantity, preview = true) {
    let postBody = orderStringPrefix + 'Side="2" ' + orderStringMiddle1 + symbol + orderStringMiddle2 + quantity + orderStringSuffix
    let urlSuffix = '/accounts/' + config.account_id

    urlSuffix += preview ? '/orders/preview.xml' : '/orders.xml'

    return postToTradeking(postBody, urlSuffix)
  }
}

var tradekingConsumer = new oauth.OAuth(
  'https://developers.tradeking.com/oauth/request_token',
  'https://developers.tradeking.com/oauth/access_token',
  config.consumer_key,
  config.consumer_secret,
  '-1.0',
  null,
  'HMAC-SHA1'
)

async function postToTradeking (postBody, apiSuffix) {
  return new Promise(function (resolve, reject) {
    tradekingConsumer.post(
      config.api_url + apiSuffix,
      config.access_token,
      config.access_secret,
      postBody,
      'text/xml',
      function (err, data, res) {
        if (data != null) {
          let jsonResponse = JSON.parse(xmljs.xml2json(data, { compact: true, spaces: 2 }))

          if (jsonResponse.response.error._text === 'Success') {
            resolve(jsonResponse)
          } else {
            reject(jsonResponse)
          }
        } else {
          reject(err)
        }
      }
    )
  })
}
