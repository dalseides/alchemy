var iex = require('iexcloud_api_wrapper')
var moment = require('moment')

module.exports =
{
  fetch: async function (ticker, date) {
    if (!date) {
      date = this.lastDay()
    }
    let data = await iex.history(ticker, { date: date.format('YYYYMMDD') })
    return data
  },
  lastDay: async function () {
    let resp = await iex.iexApiRequest('/ref-data/us/dates/trade/last')
    return moment(resp[0]['date'], 'YYYY-MM-DD')
  }
}
