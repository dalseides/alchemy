Currently, to run, in the top-level directory run:

```
node scheduler/scheduler.js
```

You **will** need to have created a .env file in the project root folder following the format of the .sample_env file in the same directory; the variables presented there currently correspond to fields of the same names in the Ally Invest (Tradeking) developer API account configuration.

# alchemy performs algorithmic backtesting and trading.

## 1. alchemy must support back-testing of an algorithm, given as an input, against historic trading data.

alchemy will accept, as input, an algorithm in the form of a closure. That closure must include any parameters it needs to run (i.e. the short and long lengths for a simple moving average crossover algorithm) at the time it is supplied to the alchemy algorithm runner. That runner can be used to perform trading (see 2.) or backtesting against specified test data. 

### Initial iterations will supply the test data as an additional input. This may retain or support or be deprecated as the project matures. 

The desired data to back-test over includes the most recent ten years (rolling) of pricing data on a per-minute level for all securities traded on the NYSE. If the project evolves to include support for trading options, futures, or other vehicles, there will be a need to store data for such vehicles as well.

Given the vast quantity of data the project aspires to make available for back-testing, passing all data to be tested as an input is infeasible.

### Later iterations of the project will support the configuration and use of an external data store from which to pull data. 

This may be a database whose configuration and operation is maintained as a subcomponent of this project, or it may be a vendor's API. Whatever external data store is chosen must enable low-latency, scalable, cost-effective, concurrent testing. A scheduler will be responsible for connecting subsets of data from that external data store with components of the in-operation back-testing algorithm(s).

## 2. alchemy must support the use of an algorithm, given as input, to perform trades against a supported broker.

The initial broker being designed towards is Ally Invest, using their Tradeking API. It is unlikely the original developers will add or maintain support for any brokers they do not themselves use; however, pull requests to add support for additional brokers will be accepted assuming they meet project standards for inclusion.

### For more data, see the project wiki.
