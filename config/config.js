require('dotenv').config()

module.exports =
{
  // Tradeking stuffs
  api_url: process.env.TRADEKING_API_URL,
  account_id: process.env.TRADEKING_ACCOUNT_ID,
  consumer_key: process.env.TRADEKING_CONSUMER_KEY,
  consumer_secret: process.env.TRADEKING_CONSUMER_SECRET,
  access_token: process.env.TRADEKING_ACCESS_TOKEN,
  access_secret: process.env.TRADEKING_ACCESS_SECRET
}
