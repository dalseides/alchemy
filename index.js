module.exports =
{
  config: require('./config/config.js'),
  analysisRunner: require('./analysis_runner/analysis_runner_prototype.js'),
  dataFeed: require('./data_feed/iex.js'),
  broker: require('./broker_communication/tradeking.js')
}
