const Moment = require('moment')

// LoggerOutputs
const ConsoleLogger = require('./logOutputs/consoleLogger')
const CsvLogger = require('./logOutputs/csvLogger')

class Logger {
  constructor (outputs) {
    this.outputs = outputs
    this.loggers = []
    if ('console' in outputs) {
      this.loggers.push(new ConsoleLogger())
    }

    if ('csv' in outputs) {
      this.loggers.push(new CsvLogger(outputs.csv))
    }
  }
  logAction (system, message, timestamp = new Moment()) {
    for (let logger of this.loggers) {
      logger.log(system, message, timestamp)
    }
  }
}

module.exports = Logger
