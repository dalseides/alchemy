var Logger = require('./logger')

class SignalLogger extends Logger {
  /**
     * Log a buy signal
     * @param {String} ticker
     * @param {Number} quantity
     * @param {Number} price
     */
  logBuy (ticker, quantity, price) {
    this.logAction('signal', {
      type: 'buy',
      ticker,
      quantity,
      price
    })
  }

  /**
     * Log a sell signal
     * @param {String} ticker
     * @param {Number} quantity
     * @param {Number} price
     */
  logSell (ticker, quantity, price) {
    this.logAction('signal', {
      type: 'sell',
      ticker,
      quantity,
      price
    })
  }
}

module.exports = SignalLogger
