const csvStringify = require('csv-stringify/lib/sync')
const fs = require('fs')
const Moment = require('moment')

class CsvLogger {
  /**
     * @typedef {Object} CsvLoggerOptions
     * @property {String[]} headers
     * @property {String} filename
     */
  /**
     * Initialize a new CSV logger with headers
     * @constructor
     * @param {CsvLoggerOptions} options
     */
  constructor ({ headers, filename }) {
    if (fs.existsSync(filename)) {
      throw new Error('The filename specified already exists')
    }

    this.filename = filename
    this.headers = headers

    // Write headers
    this.writeLineToCsv(
      ['timestamp', 'subsystem', ...headers]
    )
  }
  /**
     * Add a log line
     * @param  {String} subsystem
     * @param  {String} message
     * @param  {Moment} timestamp
     */
  log (subsystem, message, timestamp = new Moment()) {
    this.writeLineToCsv(
      [
        timestamp.toISOString(),
        subsystem,
        ...this.headers.map(key => message[key] || '')
      ]
    )
  }

  /**
     * Manually write a line to the CSV
     * @param {String[]} data
     */
  writeLineToCsv (data) {
    var lineEncoded = csvStringify([data])

    fs.appendFileSync(this.filename, lineEncoded)
  }
}

module.exports = CsvLogger
