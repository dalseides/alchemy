const Moment = require('moment')

class ConsoleLogger {
  constructor (output = console) {
    this.output = output
    this.chalk = require('chalk')
  }

  log (subsystem, message = {}, timestamp = new Moment()) {
    this.output.log(
      [
        this.chalk.greenBright(timestamp.format('HH:mm')),
        this.chalk.blueBright(`${subsystem}`),
        Object.keys(message).map((key) => `${key}: ${message[key]}`).join('; ')
      ].join(' ')
    )
  }
}

module.exports = ConsoleLogger
